package com.demo.core.impl;
import com.demo.core.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Reference;

import javax.jcr.Repository;
/**
 * Created by jimmy on 2017-03-07.
 */
@Component(immediate = true, metatype = true, label = "Hello Bundle")
@Service(value = RepositoryService.class)
public class RepositoryServiceImpl implements RepositoryService{

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryServiceImpl.class);

    @Reference
    private Repository repository;

    @Override
    public String getRepositoryName(){
        return "RepositoryServiceImpl modification made in Intellij: >> " + repository.getDescriptor(Repository.REP_NAME_DESC);
    }

    @Override
    public String getSomeMessage() {
        String message_result = "Identifier Stability: " + repository.getDescriptor(Repository.IDENTIFIER_STABILITY);
        message_result = message_result + " REP_VENDOR_URL_DESC: " + repository.getDescriptor(Repository.REP_VENDOR_URL_DESC);
        return message_result;
    }

    @Activate
    protected void activate(){
        LOGGER.info("Service Activated");
    }

    @Deactivate
    protected void deactivate(){
        LOGGER.info("Service deactivated");
    }
}
